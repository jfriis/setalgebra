.PHONY: install update-deps upgrade test pre_test docker_test docker html

# You can set ?= variables from the command line.
SHELL = /bin/bash
VIRT ?= virt
PYTHON ?= python3
SSH_KEY_FILE ?= ~/.ssh/id_rsa


install $(VIRT):
	$(PYTHON) -m venv $(VIRT)
	$(VIRT)/bin/pip install --upgrade pip setuptools pip-tools
	$(VIRT)/bin/pip-sync requirements/dev.txt
	$(VIRT)/bin/pip install --editable .

update-deps: $(VIRT)
	$(VIRT)/bin/pip install --upgrade pip setuptools pip-tools
	. $(VIRT)/bin/activate && cd requirements && $(MAKE) $@

upgrade: update-deps install

test: $(VIRT) docker_test
	. $(VIRT)/bin/activate && cd tests && pytest -v

html: $(VIRT)
	cd docs && $(MAKE) $@
