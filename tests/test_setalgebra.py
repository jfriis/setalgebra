import os
import tempfile
import pytest
import setalgebra

@pytest.fixture()
def s1():
    return set(["1", "2", "3"])


@pytest.fixture()
def s2():
    return set(["3", "4", "5"])


@pytest.fixture()
def s3():
    return set(["1", "3", "5"])


@pytest.fixture()
def s4():
    return set(["3", "5"])


def write(s):
    f, tmpfile = tempfile.mkstemp()
    with open(tmpfile, 'w') as handle:
        handle.write("\n".join(s) + '\n')
    return tmpfile


@pytest.fixture()
def file1(s1):
    tmpfile = write(s1)
    yield tmpfile
    os.unlink(tmpfile)


@pytest.fixture()
def file2(s2):
    tmpfile = write(s2)
    yield tmpfile
    os.unlink(tmpfile)


@pytest.fixture()
def file3(s3):
    tmpfile = write(s3)
    yield tmpfile
    os.unlink(tmpfile)


@pytest.fixture()
def file4(s4):
    tmpfile = write(s4)
    yield tmpfile
    os.unlink(tmpfile)


def test_parse_set(s1, file1):
    assert setalgebra.parse_set(s1) is s1
    assert setalgebra.parse_set(file1) == s1


def test_difference(s1, file1, s3):
    assert setalgebra.difference(s1, s3) == s1.difference(s3)
    assert setalgebra.difference(file1, s3) == s1.difference(s3)


def test_union(s1, file1, s3):
    assert setalgebra.union(s1, s3) == s1.union(s3)
    assert setalgebra.union(file1, s3) == s1.union(s3)


def test_intersection(s1, file1, s3):
    assert setalgebra.intersection(s1, s3) == s1.intersection(s3)
    assert setalgebra.intersection(file1, s3) == s1.intersection(s3)


def test_shunting_yard(file1, file2, file3, file4):
    expression = ['(', file1, '+', file2, ')', 'x', file3, '-', file4]
    rpn = [file1, file2, '+', file3, 'x', file4, '-']
    assert setalgebra.shunting_yard(expression) == rpn


def test_reverse_polish_calculator(file1, file2, file3, file4):
    rpn = [file1, file2, '+', file3, 'x', file4, '-']
    result = set(["1"])
    assert setalgebra.reverse_polish_calculator(rpn) == result


def test_unsupported_arg(s1, file1):
    setalgebra.validate_arg(s1)
    setalgebra.validate_arg(file1)
    with pytest.raises(setalgebra.UnsupportedArg):
        setalgebra.validate_arg('filedoesnotexist')
    with pytest.raises(setalgebra.UnsupportedArg):
        setalgebra.shunting_yard(['filedoesnotexist', '+', 'alsodoesnotexist'])
