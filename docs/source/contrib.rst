============
Contributing
============


Install
=======

Installing via make gives you a development install with tests, docs, etc.

.. code::

   make install

setalgebra is added as `Development Mode <https://setuptools.pypa.io/en/latest/userguide/development_mode.html>`_ i.e. :code:`pip install --editable`.


Documentation
=============

Compile documentation manually (find output at :code:`docs/build/html/`.)

.. code::

	make html

Source files can be found under :code:`docs/source/`.


Testing
=======

Run testing with pytest.

.. code::

	make test

Tests are available under :code:`tests/`.


Requirements
============

Go to :code:`requirements/` and make your modifications to the direct project dependencies `*.in`.

Requirements are handled in ``base.in``, ``dev.in``, ``docs.in`` and ``tests.in``.

The ``.in`` files contain direct unversioned project dependencies while ``.txt`` files contains generated pinned versions (including sub-dependencies) that can be upgraded during release, see :ref:`releasing`.

``base.txt`` requirements are installed with setup.py (pip).

``dev.txt`` requirements are installed with :code:`make install`.


.. code::

   cd requirements
   make
   cd ..
   make install


.. _releasing:

Releasing
=========

Remember to upgrade dependency versions if possibly.

.. code::

   make update-deps

Or both update-deps and install using the following.

.. code::

   make upgrade

Updating versions should be accompanied by a new round of testing.


Changelog
=========

Remember to update the :doc:`changelog`.
