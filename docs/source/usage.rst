===============
Getting started
===============


Installation
============

.. code::

   pip install setalgebra-files


Usage
=====

.. code::

   setalgebra ( file1 + file2 ) x file3 - file4


.. code:: python

   import setalgebra

