======================================
Welcome to setalgebra's documentation!
======================================

Arbitrary set calculations using the shunting yard algorithm

Gitlab
   https://gitlab.com/jfriis/setalgebra

PyPI
   https://pypi.org/project/setalgebra-files/1.0.0/

ReadTheDocs
   http://setalgebra.readthedocs.io/

.. toctree::
   :maxdepth: 2
   :caption: Contents

   usage
   cli
   api
   contrib
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

